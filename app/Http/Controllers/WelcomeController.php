<?php
//This took me hours to figure out!!! like 7 hours!
namespace App\Http\Controllers;

use Faker\Factory;
use App\Models\User;
use App\Models\Post;
use Auth;

// app/Http/Controllers/WelcomeController.php

class WelcomeController extends Controller {

    public function index() {
        $faker = Factory::create();

        //$user = User::find(1);
        $user = new User;
        $user->name = $faker->name;
/*
        $user->name = "Foo";
        $user->email = "foo@bar.com";
        $user->password = "123456";
        $user->handle = "fubar";
        */

        //$user = Auth::user();


        //$faceFeed = Post::orderBy('id', 'desc')->get();
        $faceFeed = array();
        $faceFeed[0] = new Post;
        $faceFeed[0]->user = $user;
        $faceFeed[0]->content = $faker->text;
        $faceFeed[1] = new Post;
        $faceFeed[1]->user = $user;
        $faceFeed[1]->content = $faker->text;

        return view('welcome', [
                'user' => $user,
                'faceFeed' => $faceFeed
            ]);
        }

        public function getUser($userId) {

            $user = User::find($userId);
            $faceFeed = Post::where('user_id', $userId)->get();

            return view('welcome', [
                'user' => $user,
                'faceFeed' => $faceFeed
            ]);
        }

        public function getPostForm()
        {
            return view('postForm');
        }

        public function postPostForm()
        {
            $request = request();

            $this->validate($request, [
                'content' => 'required|numeric'
            ]);

            $user = request()->user();

            if (!$user) {
                dd('You must be logged in!');
            }

            $post = new Post;
            $post->content = request()->input('content');
            $post->user_id = $user->id;
            $post->save();

            return redirect('/');
        }
    }
