<?php


namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    public $imgUrl = 'http://www.escapeimages.com/wp-content/uploads/2015/11/mountains.jpg';
    public $images = [
        'https://scontent-yyz1-1.xx.fbcdn.net/v/t1.0-9/22221903_1601601816557284_8199294917999296783_n.jpg?oh=3e702af0fedce589289f1840d7c2b2cc&oe=5A9C97D0',
        'https://scontent-yyz1-1.xx.fbcdn.net/v/t1.0-9/15780720_10154934776278278_4296967386580663111_n.jpg?oh=bad5bb53926f9232efe8a003ffac2bcd&oe=5A9CA755',
        'https://scontent-yyz1-1.xx.fbcdn.net/v/t1.0-9/22221903_1601601816557284_8199294917999296783_n.jpg?oh=3e702af0fedce589289f1840d7c2b2cc&oe=5A9C97D0',
        'https://scontent-yyz1-1.xx.fbcdn.net/v/t1.0-9/22221903_1601601816557284_8199294917999296783_n.jpg?oh=3e702af0fedce589289f1840d7c2b2cc&oe=5A9C97D0',
        'https://scontent-yyz1-1.xx.fbcdn.net/v/t1.0-9/15780720_10154934776278278_4296967386580663111_n.jpg?oh=bad5bb53926f9232efe8a003ffac2bcd&oe=5A9CA755',
        'https://scontent-yyz1-1.xx.fbcdn.net/v/t1.0-9/22221903_1601601816557284_8199294917999296783_n.jpg?oh=3e702af0fedce589289f1840d7c2b2cc&oe=5A9C97D0',
        'https://scontent-yyz1-1.xx.fbcdn.net/v/t1.0-9/22221903_1601601816557284_8199294917999296783_n.jpg?oh=3e702af0fedce589289f1840d7c2b2cc&oe=5A9C97D0',
        'https://scontent-yyz1-1.xx.fbcdn.net/v/t1.0-9/15780720_10154934776278278_4296967386580663111_n.jpg?oh=bad5bb53926f9232efe8a003ffac2bcd&oe=5A9CA755',
        'https://scontent-yyz1-1.xx.fbcdn.net/v/t1.0-9/22221903_1601601816557284_8199294917999296783_n.jpg?oh=3e702af0fedce589289f1840d7c2b2cc&oe=5A9C97D0'
];

/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
protected $fillable = [
   'name', 'email', 'password', 'handle'
];
/**
 * The attributes that should be hidden for arrays.
 *
 * @var array
 */
   protected $hidden = [
       'password', 'remember_token',
   ];
}
