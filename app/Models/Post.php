<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {
    public $time = '18h';
    public $likes = '19';

    public function user()
        {
            return $this->belongsTo('App\Models\User');
        }

}
