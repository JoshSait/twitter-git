
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<div class="left-container">


<div class="user-Job">Web Developer</div>

<div class="user-Education">SAIT</div>
<div class="user-Home">Red Deer, Alberta</div>
<div class="user-Joined">Joined July 2010</div>
<div class="user-Photos">Photo's <i class="fa fa-arrow-down" aria-hidden="true"></i></div>

<div class="user-images">
    <?php

     ?>
    <?php if (/*Auth::check()*/ $user){
        foreach ($user->images as $image) { ?>
        <img src="<?php echo $image ?>" alt="">
    <?php }} ?>
</div>
</div>
