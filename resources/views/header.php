<header>
    <div class="header-img wrapper">

        <img src="<?php echo (/*Auth::check()*/ $user ? $user->imgUrl : 'notfound.png') ?>" alt="">


    </div>
        <div class="user-name"><?php echo (/*Auth::check()*/ $user ? "Welcome, ".$user->name : '') ?></div>

    <div class="profile-image">
    </div>


    <div class="data-bar flex-container wrapper">
        <div class="data-counter-wrapper flex-1 flex-container">

            <div class="data-counter">

                <div class="data-counter-title">
                    TImeline
                </div>
                <div class="data-counter-value">

                </div>
            </div>
            <div class="data-counter">
                <div class="data-counter-title">
                    About
                </div>
                <div class="data-counter-value">

                </div>
            </div>
            <div class="data-counter">
                <div class="data-counter-title">
                    Freinds
                </div>
                <div class="data-counter-value">
                    <?php echo (/*Auth::check()*/ $user ? $user->Freinds : 'no friends'); ?>
                </div>
            </div>
            <div class="data-counter">
                <div class="data-counter-title">
                    Photos
                </div>
                <div class="data-counter-value">

                </div>
            </div>
            <div class="data-counter">
                <div class="data-counter-title">
                    More
                </div>
                <div class="data-counter-value">

                </div>
            </div>
        </div>


    </div>
</header>
